// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";
import _ from "lodash";
import I18nStore from "../../../../flux/i18n/I18nStore";
import ServiceCard from "../../../../services/ServiceCard";
import MessageError from "../../../../components/errors/MessagError";
import BrandList from "./BrandList";
import CircularProgress from "material-ui/CircularProgress";
import {List, ListItem} from "material-ui/List";
import Divider from "material-ui/Divider";
import Subheader from "material-ui/Subheader";
import RaisedButton from "material-ui/RaisedButton";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------

const styles = {
    progress: {
        padding: 20,
        textAlign: 'center',
    },
    indent: {
        marginLeft: '25px',
        marginBottom: '25px'
    },
    button: {
        margin: 0
    }
};

// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class Home extends Component {

    constructor(props, context) {
        super(props, context);

        this.user = this.props.data.user || {};
        this.user_id = this.user['_id'] || '';
        this.translations = I18nStore.getTranslations();
        this.data = {};
        this.ui_body = null;

        this.state = {
            ready: false,
            loading: false,
            error_code: '',
        };
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.setState({loading: true});
        ServiceCard.trackInfo({user_id: this.user_id}, (err, data)=> {
            if (!!err) {
                console.error(err);
                this.setState({loading: false, error_code: err});
            } else {
                console.log('DATA: ', data);
                this.data = data;
                this.generateBody();
                this.setState({loading: false, ready: true});
            }
        });
    }

    componentWillUnmount() {

    }

    generateBody() {
        this.ui_body = (
            <BrandList data={this.data}/>
        );
    }

    onTapBtnExplore(event) {
        if (_.isFunction(this.props.onAction)) {
            _.debounce(this.props.onAction)(event, 'explore');
            console.log('button pressed');
        }
    }

    render() {


        // extend style
        styles.view = {
            position: 'relative',
            background: 'rgba(255, 242, 230, 0.95)',
            marginRight: 10
        };

        styles.fullpage = {
            height: window.innerHeight,
            position: 'relative',
            display:'block'
        };

        console.log(styles.fullpage);

        let ui = (
            <div>...</div>
        );

        if (!!this.state.loading) {
            ui = (
                <div style={styles.view}>
                    <div style={styles.progress}>
                        <CircularProgress />
                    </div>
                </div>
            );
        } else if (!!this.state.error_code) {
            ui = (
                <div style={styles.view}>
                    <MessageError errorCode={this.state.error_code}/>
                </div>
            );
        } else if (!!this.ui_body) {
            ui = (
                <div >
                    {this.ui_body}
                    <Divider />
                    <Subheader>{this.translations.home.discover_new_shop}</Subheader>
                    <div style={styles.indent}>
                        <RaisedButton
                            label={this.translations.home.discover_now}
                            secondary={true} style={styles.button}
                            onMouseDown={this.onTapBtnExplore.bind(this)}
                            onTouchStart={this.onTapBtnExplore.bind(this)}
                        />
                    </div>
                </div>
            );
        }

        return (
            <div style={styles.fullpage}>
                {ui}
            </div>
        );
    }
}

Home.propTypes = {
    data: React.PropTypes.object
};
Home.defaultProps = {data: {}};

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default Home;