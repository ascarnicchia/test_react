// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";
import I18nStore from "../../flux/i18n/I18nStore";
import Paper from "material-ui/Paper";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------

const styles = {
    paper: {
        padding: 20,
        textAlign: 'left',
        backgroundColor: "#ffffff",
        paddingBottom: 60
    }
};

// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class MessageError extends Component {

    constructor(props, context) {
        super(props, context);

        this.translations = I18nStore.getTranslations() || {};
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {

        const labels = {
            error: !!this.props.message
                ? this.props.message
                : !!this.props.errorCode ? this.translations.error[this.props.errorCode] : ''
        };

        let ui = (
                <Paper className="vcenter" style={styles.paper} zDepth={5}>
                    <h2>{labels.error}</h2>
                </Paper>
        );

        return ui;
    }
}

MessageError.propTypes = {
    message: React.PropTypes.string,
    errorCode: React.PropTypes.string
};
MessageError.defaultProps = {message: '', errorCode: ''};

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default MessageError;
