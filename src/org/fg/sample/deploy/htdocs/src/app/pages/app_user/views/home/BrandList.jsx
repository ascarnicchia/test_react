import React, {Component} from "react";
import {render} from "react-dom";
import _ from "lodash";
import I18nStore from "../../../../flux/i18n/I18nStore";
import {List, ListItem} from "material-ui/List";
import Subheader from "material-ui/Subheader";
import Divider from "material-ui/Divider";
import Avatar from "material-ui/Avatar";
import {lightGreen700, orange600, pinkA100, pinkA700, darkBlack} from "material-ui/styles/colors";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------

const styles = {
    name: {
        color: darkBlack,
        fontSize: '1em',
        fontWeight: 'bold'
    },
    description: {
        color: darkBlack,
        fontSize: '0.85em'
    }
};

const getLabels = function (translations) {
    return {
        day_of_week: translations.day_of_week,
        summary: translations.home.summary,
        total_visits: translations.home.total_visits,
        visited_shops: translations.home.visited_shops,
        preferred_days: translations.home.preferred_days,
        shops: translations.home.shops,
        shop_text_one: translations.home.shop_text_one,
        shop_text_more: translations.home.shop_text_more,
        shop_day_one: translations.home.shop_day_one,
        shop_day_more: translations.home.shop_day_more
    };
};

const getBrandItems = function (labels, brand_ids, brands, brands_count) {

    const items = [];
    _.forEach(brand_ids, (brand_id)=> {
        const brand = brands[brand_id];
        const brand_count = brands_count[brand_id];
        if (!!brand && !!brand_count) {
            //-- creates labels --//
            let max = 0;
            let nday = 1;
            _.forEach(brand_count.brand_count_day_of_week, (value, key)=> {
                if (value > max) {
                    max = value;
                    nday = key;
                }
            });
            const count = brand_count.brand_count_total;
            const sday = labels.day_of_week[nday];
            const tpl_text1 = count == 1 ? _.template(labels.shop_text_one) : _.template(labels.shop_text_more);
            const tpl_text2 = max == 1 ? _.template(labels.shop_day_one) : _.template(labels.shop_day_more);
            const txt1 = tpl_text1({count: count});
            const txt2 = tpl_text2({count: max, day: sday});
            //console.log(txt1, txt2);
            //-- avatar --//
            const name = brand['name'];
            const image = brand['image'];
            if (items.length > 0) {
                items.push(<Divider key={_.uniqueId()} inset={true}/>);
            }
            if (!!image) {
                items.push(
                    <ListItem
                        key={brand_id}
                        leftAvatar={<Avatar src={image}/>}
                        primaryText={<div><span style={styles.name}>{name}</span><br/><span style={styles.description}>{txt2}</span></div>}
                        secondaryText={
                                        <p>
                                          {txt1}
                                        </p>
                                      }
                        secondaryTextLines={1}
                    />
                );
            } else {
                items.push(
                    <ListItem
                        key={brand_id}
                        leftAvatar={<Avatar backgroundColor={pinkA700}>{_.toUpper(name[0])}</Avatar>}
                        primaryText={<div><span style={styles.name}>{name}</span><br/><span style={styles.description}>{txt2}</span></div>}
                        secondaryText={
                                        <p>
                                          {txt1}
                                        </p>
                                      }
                        secondaryTextLines={1}
                    />
                );
            }
        }
    });
    return items;
};

const getDayItems = function(labels, ticket_count_day_of_week) {
    let preferred_days = '';
    let sum = 0;
    const days = {};
    _.forEach(ticket_count_day_of_week, (value, key)=> {
        const day_of_week = labels.day_of_week[key];
        sum += value;
        if (!!preferred_days) {
            preferred_days += ', ';
        }
        preferred_days += day_of_week;
        days[day_of_week] = value;
    });
    const day_items = [];
    _.forEach(days, (value, key)=>{
        days[key] = _.round((value/sum)*100) + '%';
        day_items.push(
            <ListItem
                key={key}
                primaryText={<div><span style={styles.description}>{key}:</span>&nbsp;<span style={styles.name}>{days[key]}</span></div>}
            />
        );
    });
    return {
        items:day_items,
        preferred_days:preferred_days
    };
};

// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class BrandList extends Component {

    constructor(props, context) {
        super(props, context);

        this.data = this.props.data;
        this.labels = getLabels(I18nStore.getTranslations());
        this.ui_body = null;

        this.state = {};
    }

    componentWillMount() {
        this.generateBody();
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    generateBody() {

        const brand_ids = _.keys(this.data.brands || {});

        if (brand_ids.length > 0) {
            const brands = this.data.brands;
            const brands_count = this.data.brand_count;
            const items = getBrandItems(this.labels, brand_ids, brands, brands_count);
            const day_items = getDayItems(this.labels, this.data.ticket_count_day_of_week);
            
            this.ui_body = (
                <div>
                    <List>
                        <Subheader>{this.labels.summary}</Subheader>
                        <ListItem
                            leftAvatar={<Avatar backgroundColor={lightGreen700}>{_.toUpper(this.labels.total_visits[0])}</Avatar>}
                            primaryText={<div><span style={styles.description}>{this.labels.total_visits}:</span>&nbsp;<span style={styles.name}>{this.data.ticket_count}</span></div>}
                        />
                        <ListItem
                            leftAvatar={<Avatar backgroundColor={orange600}>{_.toUpper(this.labels.visited_shops[0])}</Avatar>}
                            primaryText={<div><span style={styles.description}>{this.labels.visited_shops}:</span>&nbsp;<span style={styles.name}>{brand_ids.length}</span></div>}
                        />
                        <ListItem
                            leftAvatar={<Avatar backgroundColor={pinkA100}>{_.toUpper(this.labels.preferred_days[0])}</Avatar>}
                            primaryText={<div><span style={styles.description}>{this.labels.preferred_days}:</span>&nbsp;<br/><span style={styles.name}>{day_items.preferred_days}</span></div>}
                            initiallyOpen={true}
                            primaryTogglesNestedList={true}
                            nestedItems={day_items.items}
                        />
                    </List>
                    <Divider />
                    <List>
                        <Subheader>{this.labels.shops}</Subheader>
                        {items}
                    </List>
                </div>
            );
        } else {
            // no data (TODO: implement some gui and redirect to search...)
            this.ui_body = (

                <div>
                    NO DATA
                </div>

            );
        }
    }

    render() {

        let ui = (
            <div>...</div>
        );

        if (!!this.ui_body) {
            ui = this.ui_body;
        }

        return ui;
    }
}

BrandList.propTypes = {
    data: React.PropTypes.object
};
BrandList.defaultProps = {data: {}};

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default BrandList;