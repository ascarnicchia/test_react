// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from "react";
import {render} from "react-dom";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class Explore extends Component {

    constructor(props, context) {
        super(props, context);

   
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }


    render() {

        const styles = {
            view:{
                height:window.innerHeight,
                backgroundColor:'rgba(179, 220, 74, 0.85)'
            }
        };
        
        let ui = (
            <div style={styles.view}>
                EXPLORE
                {this.props.children}
            </div>
        );


        return ui;
    }
}

Explore.propTypes = {
    data: React.PropTypes.object
};
Explore.defaultProps = {data: {}};

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default Explore;
