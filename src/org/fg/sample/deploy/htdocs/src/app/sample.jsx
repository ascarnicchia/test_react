import React, {Component} from "react";
import {render} from "react-dom";
import {Router, Route, Redirect, Link, IndexRoute, hashHistory, browserHistory} from "react-router";


// ------------------------------------------------------------------------
//                      components
// ------------------------------------------------------------------------

const Main = React.createClass({

    componentDidMount() {
        hashHistory.push('/about'); // navigate to about

        let elem = document.getElementById('loader');
        elem.parentNode.removeChild(elem);
    },

    render() {
        return (
            <div>
                <h1>App {navigator.language}</h1>
                <ul>
                    <li><Link to="/about">About</Link></li>
                    <li><Link to="/inbox">Inbox</Link></li>
                </ul>
                {this.props.children}
            </div>
        )
    }
});

const Dashboard = React.createClass({
    render() {
        return (
            <div>
                <h1>Dashboard</h1>
                <ul>
                    <li><Link to="/about">About</Link></li>
                    <li><Link to="/inbox">Inbox</Link></li>
                </ul>
                {this.props.children}
            </div>
        )
    }
});

/**
 const About = React.createClass({
    render() {
        return <h3>About</h3>
    }
}); **/

class About extends Component {
    render() {
        return (
            <section>
                <h3>About</h3>
                <p>This is about section</p>
                <div className="row">
                    <form className="col s12">
                        <div className="row">
                            <div className="input-field col s6">
                                <input placeholder="Placeholder" id="first_name" type="text" className="validate"/>
                                    <label for="first_name">First Name</label>
                            </div>
                            <div className="input-field col s6">
                                <input id="last_name" type="text" className="validate"/>
                                    <label for="last_name">Last Name</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input disabled value="I am not editable" id="disabled" type="text" className="validate"/>
                                    <label for="disabled">Disabled</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="password" type="password" className="validate"/>
                                    <label for="password">Password</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="email" type="email" className="validate"/>
                                    <label for="email">Email</label>
                            </div>
                        </div>
                    </form>
                </div>
                {this.props.children}
            </section>
        )
    }
}

const Inbox = React.createClass({
    render() {
        return (
            <div>
                <h2>Inbox</h2>
                {this.props.children || "Welcome to your Inbox"}
            </div>
        )
    }
});

const Message = React.createClass({
    render() {
        return <h3>Message {this.props.params.id}</h3>
    }
});

// ------------------------------------------------------------------------
//                      a p p
// ------------------------------------------------------------------------



class App extends Component {
    render() {

        return (
            <Router history={hashHistory}>
                <Route path="/" component={Main}>
                    {/* Show the dashboard at / */}
                    <IndexRoute component={Dashboard}/>

                    <Route path="about" component={About}/>

                    <Route path="inbox" component={Inbox}>
                        {/* Redirect /inbox/messages/:id to /messages/:id */}
                        <Redirect from="messages/:id" to="/messages/:id"/>
                    </Route>

                    {/* Use /messages/:id instead of /inbox/messages/:id */}
                    <Route component={Inbox}>
                        <Route path="messages/:id" component={Message}/>
                    </Route>
                </Route>
            </Router>
        )
    }
}

// ------------------------------------------------------------------------
//                      e x p o r t s
// ------------------------------------------------------------------------

export default App;
