package org.fg.sample.app.webserver;



import org.fg.sample.app.webserver.api.ApiUtil;
import org.lyj.ext.netty.server.web.HttpServerConfig;
import org.lyj.ext.netty.server.web.handlers.impl.RoutingHandler;

/**
 * Internal Api router
 */
public class ApiRouter
        extends RoutingHandler {

    // ------------------------------------------------------------------------
    //                      c o n s t
    // ------------------------------------------------------------------------

    private static final String PATH_API = "/api";

    private static final String PATH_API_UTIL = PATH_API.concat("/util");



    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------



    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    protected ApiRouter(final HttpServerConfig config) {
        super(config);



        this.init();
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------

    private void init() {

        super.all(PATH_API.concat("/version")).handler(ApiUtil::version);

        //-- util --//
        super.all(PATH_API_UTIL.concat("/version")).handler(ApiUtil::version);
        super.all(PATH_API_UTIL.concat("/ping")).handler(ApiUtil::ping);
        super.get(PATH_API_UTIL.concat("/redirect/:url")).handler(ApiUtil::redirect);


    }

    // ------------------------------------------------------------------------
    //                      S T A T I C
    // ------------------------------------------------------------------------

    public static ApiRouter create(final HttpServerConfig config) {
        return new ApiRouter(config);
    }


}
