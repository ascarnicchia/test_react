package org.fg.sample.app.webserver.api;



import org.fg.sample.app.IConstants;
import org.lyj.commons.util.PathUtils;
import org.lyj.commons.util.StringUtils;
import org.lyj.ext.netty.server.web.controllers.routing.RoutingContext;

public class ApiUtil {

    public static void ping(final RoutingContext context) {
        context.writeJson("true");
    }

    public static void version(final RoutingContext context) {
        context.writeJson("{\"app\":\"" + IConstants.APP_VERSION + "\"}");
    }

    public static void redirect(final RoutingContext context) {
        final String raw_url = context.getParam("url");
        if (StringUtils.hasText(raw_url)) {
            final String url = PathUtils.hasProtocol(raw_url) ? raw_url : "http://" + raw_url;
            context.writeRedirect(url);
        }
    }
}
