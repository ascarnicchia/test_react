package org.fg.sample.app.webserver.api;


import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import org.bson.Document;
import org.fg.sample.app.controllers.TokenController;
import org.json.JSONObject;
import org.lyj.commons.Delegates;
import org.lyj.commons.logging.AbstractLogEmitter;
import org.lyj.commons.util.ConversionUtils;
import org.lyj.commons.util.FormatUtils;
import org.lyj.commons.util.StringUtils;
import org.lyj.ext.mongo.LyjMongo;
import org.lyj.ext.mongo.utils.LyjMongoObjects;
import org.lyj.ext.netty.server.web.controllers.routing.RoutingContext;

import java.util.List;

/**
 * Common API superclass
 */
public abstract class AbstractApi
        extends AbstractLogEmitter {

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    private static final String DATABASE = "funnygain";

    private static final String EMPTY_ITEM = ""; // "{\"_id\":0}"
    private static final String EMPTY_LIST = ""; // "[]"

    final static String PARAM_APP_TOKEN = "app_token";
    final static String PARAM_FILTER = "filter";
    final static String PARAM_SKIP = "skip";
    final static String PARAM_LIMIT = "limit";
    final static String PARAM_SORT = "sort";

    // ------------------------------------------------------------------------
    //                      f i e l d s
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    //                      c o n s t r u c t o r
    // ------------------------------------------------------------------------

    public AbstractApi() {

    }

    // ------------------------------------------------------------------------
    //                      p r o t e c t e d
    // ------------------------------------------------------------------------


    protected void getDatabase(final Delegates.SingleResultCallback<MongoDatabase> callback) {
        LyjMongo.getInstance().getDatabase(DATABASE, callback);
    }

    protected void getCollection(final String name, final Delegates.SingleResultCallback<MongoCollection> callback) {
        this.getDatabase((err, database) -> {
            if (null == err) {
                final MongoCollection collection = database.getCollection(name);
                if (null != collection) {
                    Delegates.invoke(callback, null, collection);
                } else {
                    Delegates.invoke(callback, new Exception("Collection not found: '" + name + "'"), null);
                }
            } else {
                Delegates.invoke(callback, err, null);
            }
        });
    }

    protected void auth(final String token, final Delegates.SingleResultCallback<Boolean> callback) {
        if (StringUtils.hasText(token)) {
            //VALIDATE TOKEN LOOKING FOR APPS
            TokenController.instance().auth(token, (err, exists) -> {
                if (null != err) {
                    Delegates.invoke(callback, err, false);
                } else {
                    if (!exists) {
                        super.warning("auth", FormatUtils.format("INVALID TOKEN '%s'", token));
                    }
                    Delegates.invoke(callback, null, exists);
                }
            });
        } else {
            Delegates.invoke(callback, new Exception("Missing Token"), false);
        }
    }

    protected String getParamToken(final RoutingContext context) {
        return getParam(context, PARAM_APP_TOKEN);
    }

    protected Document getParamFilter(final RoutingContext context) {
        final String sfilter = this.getParam(context, PARAM_FILTER);
        return StringUtils.isJSONObject(sfilter) ? Document.parse(sfilter) : new Document();
    }

    protected Document getParamSort(final RoutingContext context) {
        final String ssort = this.getParam(context, PARAM_SORT);
        return StringUtils.isJSONObject(ssort) ? Document.parse(ssort) : null;
    }

    protected int getParamSkip(final RoutingContext context) {
        return ConversionUtils.toInteger(this.getParam(context, PARAM_SKIP));
    }

    protected int getParamLimit(final RoutingContext context) {
        return ConversionUtils.toInteger(this.getParam(context, PARAM_LIMIT));
    }

    protected String getParam(final RoutingContext context,
                              final String paramName) {
        return this.getParam(context, paramName, "");
    }

    protected String getParam(final RoutingContext context,
                              final String paramName,
                              final String defaultValue) {
        String result = "";
        if (null != context) {
            result = context.params().getString(paramName);
            if (!StringUtils.hasText(result)) {
                result = context.params().getString(StringUtils.replace(paramName, "_", ""));
            }
            if (!StringUtils.hasText(result)) {
                result = defaultValue;
            }
        }
        return result;
    }

    protected void writeError(final RoutingContext context, final Throwable t) {
        this.writeError(context, t, "");
    }

    protected void writeError(final RoutingContext context, final Throwable t, final String methodName) {
        final String error = t.getMessage();
        final JSONObject json = new JSONObject();
        json.putOpt("error", error);
        context.writeJson(json);
    }

    protected void writeInternalError(final RoutingContext context, final Throwable t) {
        this.writeInternalError(context, t, "");
    }

    protected void writeInternalError(final RoutingContext context, final Throwable t, final String methodName) {
        context.writeInternalServerError(t);
        if (StringUtils.hasText(methodName)) {
            super.error(methodName, t);
        }
    }

    protected void writeErroMissingParams(final RoutingContext context, final String... names) {
        context.writeErroMissingParams(names);
    }


    protected void writeHTML(final RoutingContext context, final String html) {
        context.writeHtml(html);
    }

    protected void writeJSON(final RoutingContext context, final Object object) {
        if (object instanceof Document) {
            context.writeJson(((Document) object).toJson());
        } else if (object instanceof List) {
            context.writeJson(LyjMongoObjects.toJson(object));
        } else {
            context.writeJson(object);
        }
    }

    protected void writeJSON(final RoutingContext context, final String json) {
        context.writeJson(json);
    }

    protected void writeValue(final RoutingContext context, final Object value) {
        final JSONObject json = new JSONObject();
        json.putOpt("response", null != value ? value.toString() : "");
        context.writeJson(json);
    }

    protected void writeRawValue(final RoutingContext context, final Object value) {
        context.write(null != value ? value.toString() : "");
    }

    // ------------------------------------------------------------------------
    //                      p r i v a t e
    // ------------------------------------------------------------------------


}
