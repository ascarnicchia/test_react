package org.fg.sample.app;

import org.lyj.commons.util.CollectionUtils;

import java.util.Map;

/**
 * Application constants
 */
public interface IConstants {

    String APP_VERSION = "1.0.2";

    String HTTP_HOST_URL = "http://services.funnygain.com:4000";

    // ------------------------------------------------------------------------
    //                      DEFAULT PRIZE
    // ------------------------------------------------------------------------

    int PRIZE_INVITATION_COINS = 1000; // funny coins each invite
    int PRIZE_PARENT_BONUS_COINS = 5000;

    // ------------------------------------------------------------------------
    //                      APP TOKENS
    // ------------------------------------------------------------------------

    public static final String APP_TOKEN_HOOP = "hoop_chat_10r1a1";
    public static final String APP_TOKEN_FUNNY = "funny_gain_68j21";

    // ------------------------------------------------------------------------
    //                      E R R O R S
    // ------------------------------------------------------------------------

    String[] HANDLED_ERRORS = new String[]{
            IConstants.ERR_INVITATION_USER_EXISTS,
            IConstants.ERR_INVITATION_ALREADY_EXISTS,
            IConstants.ERR_INVITATION_NOT_ENOUGHT_CREDITS,
            IConstants.ERR_INVITATION_TO_YOU,

            IConstants.ERR_TRICK_DAY_LIMIT,

            IConstants.ERR_USER_NOT_EXISTS,
            IConstants.ERR_USER_EXISTS,
            IConstants.ERR_WRONG_LOGIN,

            IConstants.ERR_ROOM_NOT_EXISTS,
            IConstants.ERR_ROOM_EXPIRED,

            IConstants.ERR_STORE_NOT_EXISTS,

            IConstants.ERR_TOURNAMENT_NOT_EXISTS,
            IConstants.ERR_TOURNAMENT_ACCOUNT_NOT_CONFIRMED,
            IConstants.ERR_TOURNAMENT_ACCOUNT_NOT_AGE,
            IConstants.ERR_TOURNAMENT_ACCOUNT_NOT_COMPLETED

    };

    //-- system --//
    String ERR_500 = "err_500";
    String ERR_MAILER_DOWN = "err_mailer_down";

    //-- login --//
    String ERR_USER_EXISTS = "err_user_exists"; // user already exists
    String ERR_WRONG_LOGIN = "err_wrong_login";  // wrong username or password
    String ERR_USER_NOT_EXISTS = "err_user_not_exists";  // reset password error

    //-- gain --//
    String err_not_enough_credits = "err_not_enough_credits";  // reset password error
    String err_already_won = "err_already_won"; // coupon already won
    String err_already_collected = "err_already_collected";// coupon already won
    String err_fidelity_not_exists = "err_fidelity_not_exists";  // fidelity does not exists
    String err_stamp_not_exists = "err_stamp_not_exists";

    //-- chat --//
    String ERR_ROOM_NOT_EXISTS = "err_room_not_exists";
    String ERR_ROOM_EXPIRED = "err_room_expired";

    //-- store --//
    String ERR_STORE_NOT_EXISTS = "err_store_not_exists";

    //-- tournaments --//
    String ERR_TOURNAMENT_NOT_EXISTS = "err_tournament_not_exists";
    String ERR_TOURNAMENT_ACCOUNT_NOT_CONFIRMED = "err_tournament_account_not_confirmed";
    String ERR_TOURNAMENT_ACCOUNT_NOT_COMPLETED = "err_tournament_account_not_completed";
    String ERR_TOURNAMENT_ACCOUNT_NOT_AGE = "err_tournament_account_not_age";

    //-- invitation --//
    String ERR_INVITATION_ALREADY_EXISTS = "err_invitation_already_exists";
    String ERR_INVITATION_USER_EXISTS = "err_invitation_user_exists";
    String ERR_INVITATION_NOT_ENOUGHT_CREDITS = "err_invitation_not_enought_credits";
    String ERR_INVITATION_TO_YOU = "err_invitation_to_you";

    //-- tricks --//
    String ERR_TRICK_DAY_LIMIT = "err_trick_day_limit";

    //-- brand --//
    String ERR_BRAND_NOT_EXISTS = "err_brand_not_exists";

    //-- brand --//
    String ERR_REWARD_NOT_EXISTS = "err_reward_not_exists";
    String ERR_REWARD_NOT_ENOUGHT_GEMS = "err_reward_not_enought_gems";

    //-- card --//
    String ERR_CARD_NOT_EXISTS = "err_card_not_exists";
    String ERR_CARD_USER_NOT_ASSIGNED = "err_card_user_not_assigned";
    String ERR_CARD_USER_ALREADY_ASSIGNED = "err_card_user_already_assigned";
    String ERR_CARD_MOBILE_NOT_VALID_NUMBER = "err_card_mobile_not_valid_number";

    // ------------------------------------------------------------------------
    //                      F I E L D S
    // ------------------------------------------------------------------------

    public static final String DATE_LONG = "date_long";

    // ------------------------------------------------------------------------
    //                      C O N S T
    // ------------------------------------------------------------------------

    public static Map<String, String> OS_TYPES = CollectionUtils.toMap("1","ios", "3", "android");

    public static final String ID_SEP = "_"; // default separator for fields




}
