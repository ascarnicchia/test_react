var PATHS = {
    app: './src/org/fg/sample/deploy/htdocs/src',
    output: 'src/org/fg/sample/deploy/htdocs/build'
};

module.exports = {

    entry:  PATHS.app,

    resolve: {
        extensions: ['', '.js', '.jsx', '.es6', '.scss']
    },

    output: {
        path:     PATHS.output,
        filename: 'bundle.js'
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                //include: "./src/org/lyj/desktopfences/deploy/htdocs/src",
                loader: 'babel',
                query:{
                    presets: ['react', 'es2015']
                }

            },
            { test: /\.css$/, loader: "style-loader!css-loader" },
            { test: /\.png$/, loader: "url-loader?limit=100000" },
            { test: /\.jpg$/, loader: "file-loader" },
            { test: /\.scss$/, loaders: ["style", "css", "sass"] }
        ]
    }

};