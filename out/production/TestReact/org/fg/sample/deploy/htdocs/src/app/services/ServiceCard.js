// ------------------------------------------------------------------------
//                      i m p o r t s
// ------------------------------------------------------------------------

import BaseService from "./BaseService";

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------

const PATH = '/card';

// ------------------------------------------------------------------------
//                      c l a s s
// ------------------------------------------------------------------------

class ServiceCard extends BaseService {

    constructor() {
        super();
    }

    exists(params, callback) {
        const card_id = params.card_id;
        if (!!card_id) {
            super.post(PATH + '/exists', {card_id: card_id}, callback);
        } else {
            callback('err_missing_parameter');
        }
    }

    cardFree(params, callback) {
        const card_id = params.card_id;
        if (!!card_id) {
            super.post(PATH + '/card_free', {card_id: card_id}, callback);
        } else {
            callback('err_missing_parameter');
        }
    }

    cardRegister(params, callback) {
        const card_id = params.card_id;
        const lang = params.lang;
        const name = params.name;
        const mobile = params.mobile;
        // console.log("CALLING REGISTER", card_id, lang, name, mobile);
        if (!!card_id && !!name && !!mobile) {
            super.post(PATH + '/card_register', {lang:lang, card_id: card_id, name: name, mobile: mobile}, callback);
        } else {
            callback('err_missing_parameter');
        }
    }

    getOwner(params, callback) {
        const card_id = params.card_id;
        if (!!card_id) {
            super.post(PATH + '/get_owner', {card_id: card_id}, callback);
        } else {
            callback('err_missing_parameter');
        }
    }

    trackInfo(params, callback) {
        const user_id = params.user_id;
        if (!!user_id) {
            super.post(PATH + '/track_info', {user_id: user_id}, callback);
        } else {
            callback('err_missing_parameter');
        }
    }
}

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default new ServiceCard();