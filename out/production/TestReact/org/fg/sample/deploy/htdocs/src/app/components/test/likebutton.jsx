// ------------------------------------------------------------------------
//                      i m p o r t s
// ------------------------------------------------------------------------

import React, {Component} from "react";

// ------------------------------------------------------------------------
//                      c l a s s
// ------------------------------------------------------------------------


const btn_style = {
    bottom: '45px',
    right: '24px'
};

class LikeButton extends Component {

    static getInitialState() {
        return {liked: false};
    }

    handleClick(event) {
        this.setState({liked: !this.state.liked});
    }

    render() {

        return (
            <div>
                
                <div className="fixed-action-btn" style={btn_style}>
                    <a className="btn-floating btn-large waves-effect waves-circle waves-light">
                        <i className="large material-icons">mode_edit</i>
                    </a>
                    <ul>
                        <li><a className="btn-floating red" onClick={this.handleClick}><i className="material-icons">insert_chart</i></a>
                        </li>
                        <li><a className="btn-floating yellow darken-1"><i
                            className="material-icons">format_quote</i></a>
                        </li>
                        <li><a className="btn-floating green"><i className="material-icons">publish</i></a></li>
                        <li><a className="btn-floating blue"><i className="material-icons">attach_file</i></a></li>
                    </ul>
                </div>
            </div>
        )
    }
}

// ------------------------------------------------------------------------
//                      e x p o r t s
// ------------------------------------------------------------------------

export default LikeButton;