// ------------------------------------------------------------------------
//                      b o o t s t r a p p e r
// ------------------------------------------------------------------------

import polyfill from "./lib/Polyfill";
import React from "react";
import ReactDOM from "react-dom";
import App from "./app/Application";
import $ from "jquery";
import injectTapEventPlugin from "react-tap-event-plugin";


//-- init tap handler --//
injectTapEventPlugin();

const jq = $; // avoid IDE import optimization
//const mt = m; // avoid IDE import optimization
const p = polyfill;

ReactDOM.render(
    <App />,
    document.getElementById('app')
);