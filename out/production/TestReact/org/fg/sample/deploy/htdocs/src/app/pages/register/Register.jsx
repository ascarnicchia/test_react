// ------------------------------------------------------------------------
//                      i m p o r t
// ------------------------------------------------------------------------

import React, {Component} from 'react';
import {render} from 'react-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {IntlProvider, FormattedMessage} from 'react-intl';
import I18nStore from '../../flux/i18n/I18nStore';
import ServiceCard from '../../services/ServiceCard';
import {Router, Route, Redirect, Link, IndexRoute, hashHistory} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';

// ------------------------------------------------------------------------
//                      c o n s t
// ------------------------------------------------------------------------

const URI_APP_USER = '/app_user/';

const styles = {
    button: {
        margin: 12,
        position: "absolute",
        right: 0
    },
    iconStyles: {
        marginRight: 24,
    },
    page: {
        width: "100%",
        height: "100%",
        position: "relative",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    paper: {
        padding: 20,
        textAlign: 'left',
        backgroundColor: "#ffffff",
        marginBottom: 50,
        paddingBottom: 60
    },
    card: {
        color: '#777777',
        marginLeft: 5
    }
};


// ------------------------------------------------------------------------
//                      c l a s s 
// ------------------------------------------------------------------------

class Register extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            card: null,
            lang: I18nStore.getLang(),
            translations: I18nStore.getTranslations(),
            mobile: '',
            name: '',
            valid_card: false,
            valid_mobile: true,
            valid_name: true,
            error_code: '',
            done: false
        };
        this.card_id = this.props.params.cardId;

    }

    componentWillMount() {
        // lookup card

        I18nStore.addChangeListener(()=> {
            this.setState({
                lang: I18nStore.getLang(),
                translations: I18nStore.getTranslations()
            });
        });
    }

    componentDidMount() {
        console.log('VALIDATE: ' + this.card_id);
        ServiceCard.cardFree({card_id: this.card_id}, (err, response)=> {
            if (!!err) {
                // some error
                console.log(err);
                this.setState({error_code: err});
            } else {
                const card = response;
                console.log("CARD:", card);
                const is_valid = !!card['_id'] && card['type'] === 'user';
                if (!is_valid) {
                    console.log('NOT VALID. CARD "' + card['_id'] + '" is of type "' + card['type'] + '"');
                    this.setState({error_code: "err_card_wrong_type"});
                } else {
                    this.setState({valid_card: is_valid, card: card, error_code:''})
                }
            }
        });
    }

    handleChangeLang(event, index, value) {
        I18nStore.setLang(value);
    }

    handleChangeMobile(value) {
        this.setState({mobile: value, valid_mobile: !!value, error_code: ''});
    }

    handleChangeName(value) {
        this.setState({name: value, valid_name: !!value, error_code: ''});
    }

    handleRegister() {
        let valid = !!this.state.mobile && !!this.state.name
            && this.state.valid_mobile && this.state.valid_name;

        if (valid) {
            // do registration
            console.log('REGISTERING');
            const card_id = this.card_id;
            const name = this.state.name;
            const lang = this.state.lang;
            const mobile = this.state.mobile;

            console.log(mobile);

            ServiceCard.cardRegister({card_id: card_id, lang: lang, name: name, mobile: mobile}, (err, data)=> {
                console.log('REGISTERED');
                if (!!err) {
                    // some error
                    console.log(err);
                    this.setState({error_code: err});
                } else {
                    console.log(data);
                    // change view
                    hashHistory.push(URI_APP_USER + card_id);
                }
            });
        } else {
            console.log('MISSING DATA');
            // check if user clicked 'register' before filling data
            if (!this.state.mobile) {
                this.setState({valid_mobile: false});
            }
            if (!this.state.name) {
                this.setState({valid_name: false});
            }
        }
    }


    render() {

        let self = this;

        const labels = {
            title: this.state.translations.register.title,
            action: this.state.translations.register.action,
            mobile: this.state.translations.register.mobile,
            mobile_hint: this.state.translations.register.mobile_hint,
            mobile_error: this.state.valid_mobile ? '' : this.state.translations.register.mobile_error,
            name: this.state.translations.register.name,
            name_hint: this.state.translations.register.name_hint,
            name_error: this.state.valid_name ? '' : this.state.translations.register.name_error,
            error: !!this.state.error_code ? this.state.translations.error[this.state.error_code] : ''
        };

        // check if there's a (server-side) validation error for mobile number
        if (!!this.state.error_code && this.state.error_code === 'err_card_mobile_not_valid_number') {
            labels.mobile_error = labels.error;
        }

        const actions = [
            <RaisedButton
                label={labels.action}
                primary={true}
                icon={<FontIcon className="material-icons" style={styles.iconStyles}>favorite</FontIcon>}
                onTouchTap={this.handleRegister}
            />
        ];

        const languages = [
            <MenuItem key="it" value={"it"} label="Italiano" primaryText="Italiano"/>,
            <MenuItem key="en" value={"en"} label="English" primaryText="English"/>
        ];


        let ui_register = (
            <div>
                <img className="bg" src="./assets/img/bg/shopping_light.jpg"/>
            </div>
        );

        if (this.state.valid_card) {
            ui_register = (
                <div>

                    <img className="bg" src="./assets/img/bg/shopping_light.jpg"/>

                    <div style={styles.page}>
                        <Paper className="vcenter" style={styles.paper} zDepth={5}>
                            <h2>{labels.title}</h2>
                            <div style={styles.card}>{this.state.card.number}</div>
                            <div>
                                <SelectField maxHeight={300} autoWidth={true} value={this.state.lang}
                                             onChange={(event, index, value)=>{self.handleChangeLang(event, index, value)}}>
                                    {languages}
                                </SelectField>
                                <br/>
                                <TextField
                                    hintText={labels.mobile_hint}
                                    floatingLabelText={labels.mobile}
                                    errorText={labels.mobile_error}
                                    type="tel"
                                    onChange={(event)=>{self.handleChangeMobile(event.target.value)}}
                                />
                                <br/>
                                <TextField
                                    hintText={labels.name_hint}
                                    floatingLabelText={labels.name}
                                    errorText={labels.name_error}
                                    fullWidth={true}
                                    onChange={(event)=>{self.handleChangeName(event.target.value)}}
                                />
                                <br/>
                                <br/>
                                <div>
                                    <RaisedButton
                                        style={styles.button}
                                        label={labels.action}
                                        primary={true}
                                        icon={<FontIcon className="material-icons" style={styles.iconStyles}>favorite</FontIcon>}
                                        onTouchTap={(event)=>{self.handleRegister()}}
                                    />
                                </div>

                            </div>
                        </Paper>
                    </div>

                </div>
            );
        } else if (!!this.state.error_code) {
            const message = labels.error || this.state.error_code;
            ui_register = (
                <div>

                    <img className="bg" src="./assets/img/bg/shopping_light.jpg"/>

                    <div style={styles.page}>
                        <Paper className="vcenter" style={styles.paper} zDepth={5}>
                            <h2>{message}</h2>
                        </Paper>
                    </div>

                </div>
            );
        }

        return (
            <ReactCSSTransitionGroup transitionName="example" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
                {ui_register}
            </ReactCSSTransitionGroup>
        )
    }

}

// ------------------------------------------------------------------------
//                      e x p o r t
// ------------------------------------------------------------------------

export default Register;